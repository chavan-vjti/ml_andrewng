function J = regression(x,y,theta)
  m = size(x,1)
  predictions = x*theta
  sqerror = (y - predictions).^2
  J = (1/(2*m))*sum(sqerror)
endfunction
